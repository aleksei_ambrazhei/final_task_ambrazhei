import mysql.connector as mysql
import docker
import time


def start_docker():
    client = docker.from_env()
    print('Starting docker container...')
    return client, client.containers.run("mysql", name="mysql_car_sharing", detach=True,
                                         ports={3306: 3306}, environment=[
            "MYSQL_ROOT_PASSWORD=99len765",
            "MYSQL_USER=user",
            "MYSQL_PASSWORD=Qwerty@123",
            "MYSQL_DATABASE=car_sharing"
        ])


def create_tables():
    time.sleep(15)
    db = mysql.connect(host='localhost',
                       user='user',
                       passwd='Qwerty@123',
                       database='car_sharing')

    cursor = db.cursor()

    cursor.execute("CREATE TABLE cars (car_types VARCHAR(255), mark VARCHAR(255), "
                   "description VARCHAR(255), vehicle_number INT, price INT, "
                   "transmission VARCHAR(255), car_mileage INT, drive_gear VARCHAR(255))")

    query = "INSERT INTO cars (car_types, mark, description, vehicle_number, price, transmission," \
            " car_mileage, drive_gear) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

    values = [

        ("passenger", "volvo", "red", "8775", "150", "auto", "30000", "rear drive"),

        ("truck", "mercedes", "blue", "5500", "300", "hand", "130000", "rear drive"),

        ("passenger", "nissan", "green", "2244", "200", "auto", "50000", "front-wheel drive")]

    cursor.executemany(query, values)
    db.commit()

    cursor.execute("CREATE TABLE buyers (id_buyer INT, name VARCHAR(255), "
                   "phone INT, address VARCHAR(255))")

    query = "INSERT INTO buyers (id_buyer, name, phone, address) VALUES (%s, %s, %s, %s)"

    values = [("111", 'pasha', '100232233', 'New York City, Wall street av, 14'),

              ("112", 'sergei', '291234343', 'Minsk, Pobeditelei str, 123'),

              ("113", 'masha', '2325577', 'Moscow, Nevskuy ave, 12')]
    cursor.executemany(query, values)
    db.commit()

    cursor.execute("CREATE TABLE history (id_rent INT, id_buyer INT, "
                   "vehicle_number INT, start_date DATE, finish_date DATE)")

    query = "INSERT INTO history (id_rent, id_buyer, vehicle_number, " \
            "start_date, finish_date) VALUES (%s, %s, %s, %s, %s)"

    values = [("4444", '111', '8775', '2020-10-05', '2020-10-10'),

              ("4445", '112', '5500', '2019-08-05', '2019-09-05'),

              ("4446", '113', '2244', '2021-02-05', '2021-02-15')]

    cursor.executemany(query, values)
    db.commit()
    return db, cursor


def testing_data(db, cursor):
    # a.	Вывести список всех машин и их гос номер
    cursor.execute("SELECT car_types, mark, vehicle_number FROM cars")
    print('all cars', cursor.fetchall())
    # b.	Вывести список машин и их гос номер, за исключением марки “Ниссан”
    cursor.execute("SELECT mark, vehicle_number FROM cars WHERE mark <> 'nissan'")
    print('cars, except nissan', cursor.fetchall())
    # c.	Вывести список машин и их гос номер, с пробегом от 100к до 150к км
    cursor.execute(
        "SELECT mark, vehicle_number FROM cars WHERE car_mileage BETWEEN 100000 AND 150000")
    print('cars car_mileage between 100 and 150', cursor.fetchall())
    # d.	Добавить пару машин в таблицу
    query = "INSERT INTO cars (car_types, mark, description, vehicle_number, price, transmission," \
            " car_mileage, drive_gear) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    values = [
        ("passenger", "volga", "black", "8375", "150", "auto", "30000", "rear drive"),
        ("truck", "mercedes", "blue", "5200", "300", "hand", "130000", "rear drive"),
        ("truck", "reno", "green", "2644", "200", "hand", "180000", "front-wheel drive")]
    cursor.executemany(query, values)
    db.commit()
    cursor.execute("SELECT * from cars")
    print('adding 3 cars', cursor.fetchall())
    # e.	Вывести кол-во зарегистрированных клиентов компании
    cursor.execute("SELECT * FROM buyers")
    print('clients', cursor.fetchall())
    # f.	Добавить поле “Скидка” в таблице “Покупатели”
    cursor.execute("ALTER TABLE buyers ADD COLUMN discount VARCHAR(255)")
    cursor.execute('UPDATE buyers SET discount="10" WHERE name="sergei"')
    cursor.execute('UPDATE buyers SET discount="15" WHERE name="pasha"')
    cursor.execute('UPDATE buyers SET discount="20" WHERE name="masha"')
    db.commit()
    cursor.execute("SELECT * FROM buyers")
    print('dicsount', cursor.fetchall())
    # g.	Вывести клиента с максимальной скидкой
    cursor.execute("SELECT * FROM buyers WHERE discount=(SELECT MAX(discount) FROM buyers)")
    print('max discount', cursor.fetchall())
    # h.	Найти общий пробег авто
    cursor.execute("SELECT SUM(car_mileage) FROM cars")
    print('total mileage', cursor.fetchall())
    # i.	Вывести список всех закончившихся поездок (аренд)
    cursor.execute("SELECT * FROM history WHERE finish_date < '2020-02-13'")
    print('finished rents', cursor.fetchall())
    cursor.execute("SELECT finish_date FROM history")
    print('all rents', cursor.fetchall())
    # j.	Список машин арендованных на заданную дату
    cursor.execute("SELECT * FROM history WHERE start_date = '2020-10-05'")
    print('rent on date 2020-10-05', cursor.fetchall())
    # k.	Список машин арендованных сегодня
    cursor.execute("SELECT * FROM history WHERE start_date = CURRENT_DATE()")
    print("today's rents", cursor.fetchall())
    # l.	Список машин арендованных в заданный период
    cursor.execute("SELECT * FROM history WHERE start_date BETWEEN '2020-09-09' AND '2021-02-17'")
    print('rents between', cursor.fetchall())
    # m.	Сортировать авто по цене (убыв, возр)
    cursor.execute("SELECT * FROM cars ORDER BY price ASC")
    print('sort by ASC', cursor.fetchall())
    cursor.execute("SELECT * FROM cars ORDER BY price DESC")
    print('sort by DESC', cursor.fetchall())
    # n.	Найти средний пробег всех авто
    cursor.execute("SELECT AVG(car_mileage) FROM cars")
    print('avarege car_mileage', cursor.fetchall())
    # o.	Удалить из таблицы “Транспортные средства” машины марки Ниссан
    cursor.execute("DELETE FROM cars WHERE mark='nissan'")
    db.commit()
    cursor.execute("SELECT * from cars")
    print('db without nissan', cursor.fetchall())
    # p.	Удалите все данные из таблицы “Покупатели”
    cursor.execute("TRUNCATE TABLE buyers")
    db.commit()
    cursor.execute("SELECT * FROM buyers")
    print('empty db buyers', cursor.fetchall())


def main():
    client, container = start_docker()
    db, cursor = create_tables()
    testing_data(db, cursor)
    while True:
        delete_container = input("Print Yes to delete container: ")
        if delete_container in ['Yes', 'yes']:
            print('\nRemoving MySQL container')
            time.sleep(2)
            print('\nRemoved')
            container.remove(v=True, force=True)
            exit(0)


if __name__ == "__main__":
    main()
