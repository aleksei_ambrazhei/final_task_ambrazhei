# This is my final task of IT Course - Python Test Automation . It contains 3 separate tasks 

## I. Testing online shop  
## II. Testing API 
## III. Creating docker container by python scripts and requests from DB 

For using this module you should make git clone this repository - [repository](https://bitbucket.org/aleksei_ambrazhei/final_task_ambrazhei/src/master/)

## Requirements 

Install requirements.txt - `pip install -r .\requirements.txt`

### I. Testing online shop  
[go_to_online_shop](http://automationpractice.com/index.php)

Directory contains dir `pages` where I've described online shop pages and 3 file with tests. (`test_cart.py`, `test_login_page.py`, `test_main_page.py`)
All the necessary information for the tests is in the file `data.xml`

a) Navigate to directory `shop_testing`

b) Use command `pytest -v` in directory shop_testing to start all tests or you can start it separate by command pytest -v "name of test file" e.g `pytest test_cart.py`, `pytes test_login_page.py`, `pytes test_main_page.py` 

c) Either you can start it from yours IDE using pytest

### II. Testing API 
[go_to_API_website](https://gorest.co.in/)

All the necessary information for the requests is in the files data.json and user_data.json 

a) Run by your favourite IDE or use command python .\gorest_api.py

b) Follow response code to make alteration in user_data.json e.g. There is possible situation when you try creating user with function def create_user(body) and put here have been used email address. To avoid getting code "422" make a few adjustments in email field of user_data.json 


### III. Creating and starting docker container by python script. 

a) Install docker from [docker](https://www.docker.com/products/docker-app)

b) Either you need pull mysql image from [mysql](https://hub.docker.com/_/mysql) 

c) Start file with your IDE or use command `python mysql_final_task.py` 








