from pages.base_page import BasePage
from selenium.webdriver.common.by import By
from pages.login_page import LoginPageLocators
from pages.base_page import root


# вроде бы нигде больше не встречается кроме первой переходы на Popular и Best seller
# решил их добавить в проверки.
class MainPageLocators:
    POPULAR = (By.CSS_SELECTOR, '.nav.nav-tabs.clearfix .homefeatured')
    BEST_SELLERS = (By.CSS_SELECTOR, '.nav.nav-tabs.clearfix .blockbestsellers')
    CART = (By.XPATH, '//a[@title="View my shopping cart"]')
    LOGIN = (By.CSS_SELECTOR, '.header_user_info .login')
    ITEMS_LIST = (By.CSS_SELECTOR, ".product-image-container .product_img_link "
                                   ".replace-2x.img-responsive")
    LOGO = (By.CSS_SELECTOR, '.logo.img-responsive')
    LIST_OF_ITEMS = (By.XPATH, "//div[@class='product-container']")
    CONTACT_US = (By.ID, 'contact-link')
    CONTACT_US_TEXT = (By.CSS_SELECTOR, '.page-heading.bottom-indent')
    SERVICE_DROPDOWN = (By.ID, 'id_contact')
    CONTACT_US_EMAIL_FIELD = (By.CSS_SELECTOR, '.form-control.grey.validate')
    ORDER_REFERENCE_FIELD = (By.CSS_SELECTOR, '.form-group.selector1 .form-control.grey')
    CONTACT_US_MESSAGE_FIELD = (By.CSS_SELECTOR, '.col-xs-12.col-md-9 .form-group .form-control')
    SEND_BUTTON = (By.CSS_SELECTOR, '.submit .button.btn.btn-default.button-medium')
    CONFIRM_SUCCESS_MESSAGE_CONTACT_US = (By.CSS_SELECTOR, '.alert.alert-success')
    ITEM_TEXT = (By.CSS_SELECTOR,
                 '.ajax_block_product.col-xs-12.col-sm-4.col-md-3.first-in-line.first-item-of'
                 '-tablet-line.first-item-of-mobile-line .product-name')
    SEARCH_QUERY = (By.CSS_SELECTOR, '.search_query')
    BUTTON_SEARCH = (By.CSS_SELECTOR, '.btn.btn-default.button-search')
    SEARCH_RESULT = (By.CSS_SELECTOR, '.right-block .product-name')


class MainPage(BasePage):
    main_page_url = 'http://automationpractice.com/index.php'

    def check_main_page_url(self):
        assert self.browser.current_url == 'http://automationpractice.com/index.php'
        assert 'My Store' in self.browser.title

    def search_element(self, *locators):
        self.browser.find_element(*locators)

    def clicker(self, *locators):
        button = self.wait_element(*locators)
        button.click()
        return self.browser, self.browser.current_url

    def log_in(self):
        button = self.wait_element(*MainPageLocators.LOGIN)
        button.click()
        self.wait_element(*LoginPageLocators.REGISTERED_EMAIL).send_keys(
            f'{root.find("email_for_registration").text}')
        self.wait_element(*LoginPageLocators.REGISTERED_PASSWD).send_keys(
            f'{root.find("password").text}')
        self.clicker(*LoginPageLocators.BUTTON_SUBMIT_LOGIN)
