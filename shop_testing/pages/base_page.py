from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import xml.etree.ElementTree as ET
import string
import random

data = ET.parse('data.xml')
root = data.getroot()


class BasePage:

    def __init__(self, browser, url):
        self.browser = browser
        self.url = url

    def open(self):
        self.browser.get(self.url)

    def wait_element(self, find_by, locator, timeout=10):
        element = WebDriverWait(self.browser, timeout).until(
            EC.element_to_be_clickable((find_by, locator)))
        return element

    def wait_element_clickable(self, find_by, locator, timeout=15):
        button = WebDriverWait(self.browser, timeout).until(
            EC.element_to_be_clickable((find_by, locator)))
        return button

    def generate_email(self):
        self.email = ''
        self.x = 0
        general_list = string.ascii_letters
        while self.x < 20:
            for i in random.choice(general_list):
                self.email += i
                self.x += 1
        use_email = self.email + '@test.com'
        return use_email
