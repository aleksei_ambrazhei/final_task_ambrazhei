from pages.base_page import BasePage
from selenium.webdriver.common.by import By


class LoginPageLocators:
    AUTHENTICATION = (By.CSS_SELECTOR, '.page-heading')
    CREATE_AN_ACCOUNT_BUTTON = (By.CSS_SELECTOR, '.btn.btn-default.button.button-medium.exclusive')
    SIGN_IN_BUTTON = (By.XPATH, '//button[@type="submit" and @id="SubmitLogin"]')
    EMAIL_FIELD = \
        (By.XPATH, '//input[@type="text" and @class="is_required validate '
                   'account_input form-control"]')

    PASSWORD_FIELD = (By.ID, '#passwd')
    EMPTY_OR_INCORRECT_EMAIL_IN_CREATE_AN_ACCOUNT = (By.XPATH, '//ol/li')
    SECOND_REGISTRATION_PAGE = (By.XPATH, '//button[@id="submitAccount"]')
    REGISTERED_EMAIL = (By.CSS_SELECTOR, '#email')
    REGISTERED_PASSWD = (By.CSS_SELECTOR, '#passwd')
    BUTTON_SUBMIT_LOGIN = (By.XPATH, '//button[@id="SubmitLogin"]')


class LoginPageRegistration:
    GENDER = (By.CSS_SELECTOR, '#uniform-id_gender1')
    FIRST_NAME = (By.CSS_SELECTOR, "#customer_firstname")
    LAST_NAME = (By.CSS_SELECTOR, "#customer_lastname")
    PASSWORD_FIELD = (By.CSS_SELECTOR, "#passwd")
    ADDRESS = (By.CSS_SELECTOR, "#address1")
    CITY = (By.CSS_SELECTOR, "#city")
    STATE = (By.XPATH, "//select[@name='id_state']")
    ZIP_CODE = (By.CSS_SELECTOR, "#postcode")
    PHONE = (By.CSS_SELECTOR, "#phone_mobile")
    ASSIGN_FUTURE_ALIAS = (By.CSS_SELECTOR, "#alias")
    REGISTER_BUTTON = (By.CSS_SELECTOR, "#submitAccount > span")
    BLOCK = (By.CSS_SELECTOR, "#noSlide")
    MY_ACCOUNT_TEXT = (By.CSS_SELECTOR, "#center_column>h1")
    SUCCESS_REGISTRATION = (By.CSS_SELECTOR, '.header_user_info .logout')


class LoginPage(BasePage):

    def should_be_login_url(self):
        assert self.browser.current_url == \
               'http://automationpractice.com/index.php?controller=authentication&back' \
               '=my-account'

    def should_be_authentication(self, *locators):
        authentication = self.wait_element(*locators)
        assert authentication

    def should_be_create_button(self, *locators):
        button = self.wait_element(*locators)
        assert 'Create an account' in button.text

    def should_be_sign_in_button(self, *locators):
        button = self.wait_element(*locators)
        assert 'Sign in' in button.text
