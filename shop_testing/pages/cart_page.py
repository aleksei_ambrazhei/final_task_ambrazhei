from pages.base_page import BasePage
from selenium.webdriver.common.by import By


class CartPageLocators:
    EMPTY_CART = (By.CSS_SELECTOR, '.alert.alert-warning')
    CART_SUMMARY = (By.CSS_SELECTOR, '.page-heading')
    BUTTON_ADD = (By.CSS_SELECTOR, '#add_to_cart .exclusive')
    WISHLIST_BUTTON_ADD = (By.ID, 'wishlist_button')
    CONFIRM_ITEM_IN_CART = (By.XPATH, '//span[@class="ajax_cart_product_txt "]')
    WISHLIST_ALERT = (By.CSS_SELECTOR, '.fancybox-error')
    WISHLIST_BUTTON_CLOSE_ALERT = (By.CSS_SELECTOR, '.fancybox-item.fancybox-close')
    PROCCEED_TO_CHECKOUT = (By.CSS_SELECTOR, '.btn.btn-default.button.button-medium')
    PROCCEED_TO_CHECKOUT_INSIDE_CART = (By.CSS_SELECTOR, '.button.btn.btn-default.'
                                                         'standard-checkout.button-medium ')
    PROCCEED_TO_CHECKOUT_INSIDE_CART_ADDRESS = (By.CSS_SELECTOR, '.cart_navigation.clearfix '
                                                                 '.button.btn.btn-default.button'
                                                                 '-medium')

    AGREE_CHECKBOX = (By.CSS_SELECTOR, '.checker')
    PAY_BY_BANK_WIRE_BUTTON = (By.CSS_SELECTOR, '.bankwire')
    CONFIRM_ORDER = (By.CSS_SELECTOR, '.cart_navigation.clearfix '
                                      '.button.btn.btn-default.button-medium')
    CONFIRMATION_ORDER_TEXT = (By.CSS_SELECTOR, '.cheque-indent .dark')


class CartPage(BasePage):

    def should_be_cart_url(self):
        assert self.browser.current_url == 'http://automationpractice.com/index.php?controller' \
                                           '=order '

    def should_be_cart_shopping_summary(self, *locators):
        cart_summary_empty = self.wait_element(*locators)
        assert cart_summary_empty

    def should_be_cart_empty(self, *locators):
        alert_empty = self.wait_element(*locators)
        assert 'Your shopping cart is empty.' in alert_empty.text

    def should_be_cart_page(self, *locators):
        self.should_be_cart_url()
        self.should_be_cart_shopping_summary(*locators)
