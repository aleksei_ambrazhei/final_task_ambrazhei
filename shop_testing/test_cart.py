from pages.main_page import MainPage, MainPageLocators
from pages.cart_page import CartPage, CartPageLocators
from pages.base_page import root


def test_cart_page_elements(open_browser):
    main_page = MainPage(open_browser, root.find("main_url").text)
    main_page.open()
    main_page.clicker(*MainPageLocators.CART)
    cart_page = CartPage(open_browser, open_browser.current_url)
    cart_page.should_be_cart_page(*CartPageLocators.CART_SUMMARY)
    cart_page.should_be_cart_empty(*CartPageLocators.EMPTY_CART)


# проверяем добавление товара в корзину
def test_adding_items(open_browser):
    main_page = MainPage(open_browser, root.find('main_url').text)
    main_page.open()
    main_page.clicker(*MainPageLocators.ITEMS_LIST)
    main_page.clicker(*CartPageLocators.BUTTON_ADD)
    confirmation = main_page.wait_element(*CartPageLocators.CONFIRM_ITEM_IN_CART)
    assert "There is 1 item in your cart." in confirmation.text


# проверяем добавление товара в wishlist
def test_adding_items_to_wishlist(open_browser):
    wish_list = MainPage(open_browser, root.find('main_url').text)
    wish_list.open()
    wish_list.log_in()
    wish_list.clicker(*MainPageLocators.LOGO)
    wish_list.clicker(*MainPageLocators.ITEMS_LIST)
    wish_list.clicker(*CartPageLocators.WISHLIST_BUTTON_ADD)
    confirm = wish_list.wait_element(*CartPageLocators.WISHLIST_ALERT)
    assert 'Added to your wishlist.' in confirm.text
    wish_list.clicker(*CartPageLocators.WISHLIST_BUTTON_CLOSE_ALERT)


# проверяем оплату и возможность покупки (весь цикл)
def test_whole_order(open_browser):
    order = MainPage(open_browser, root.find("main_url").text)
    order.open()
    order.log_in()
    test_adding_items(open_browser)
    order.clicker(*CartPageLocators.PROCCEED_TO_CHECKOUT)
    order.clicker(*CartPageLocators.PROCCEED_TO_CHECKOUT_INSIDE_CART)
    order.clicker(*CartPageLocators.PROCCEED_TO_CHECKOUT_INSIDE_CART_ADDRESS)
    order.clicker(*CartPageLocators.AGREE_CHECKBOX)
    order.clicker(*CartPageLocators.PROCCEED_TO_CHECKOUT_INSIDE_CART)
    order.clicker(*CartPageLocators.PAY_BY_BANK_WIRE_BUTTON)
    order.clicker(*CartPageLocators.CONFIRM_ORDER)
    confirm = order.wait_element(*CartPageLocators.CONFIRMATION_ORDER_TEXT)
    assert "Your order on My Store is complete." in confirm.text
