from selenium import webdriver
from pytest import fixture


@fixture
def open_browser():
    browser = webdriver.Chrome()
    yield browser
    browser.quit()
