from pages.main_page import MainPage, MainPageLocators
from pages.base_page import root
from selenium.webdriver.support.ui import Select


def test_main_page_is(open_browser):
    main_page = MainPage(open_browser, root.find("main_url").text)
    main_page.open()
    main_page.check_main_page_url()
    main_page.search_element(*MainPageLocators.POPULAR)
    main_page.search_element(*MainPageLocators.LOGIN)
    main_page.search_element(*MainPageLocators.CART)
    main_page.search_element(*MainPageLocators.BEST_SELLERS)


# проверяем наличие списка вещей на главной странице
def test_items_list(open_browser):
    main_page = MainPage(open_browser, root.find("main_url").text)
    main_page.open()
    count = len(open_browser.find_elements(*MainPageLocators.LIST_OF_ITEMS))
    assert count > 0


# проверка отправки формы contact us
def test_contact_us_form(open_browser):
    main_page = MainPage(open_browser, root.find("main_url").text)
    main_page.open()
    main_page.wait_element(*MainPageLocators.CONTACT_US).click()
    confirm = main_page.wait_element(*MainPageLocators.CONTACT_US_TEXT)
    assert "CUSTOMER SERVICE - CONTACT US" in confirm.text
    service = Select(open_browser.find_element(*MainPageLocators.SERVICE_DROPDOWN))
    service.select_by_value(root.find('Webmaster').text)
    main_page.wait_element(*MainPageLocators.CONTACT_US_EMAIL_FIELD).send_keys(
        main_page.generate_email())
    main_page.wait_element(*MainPageLocators.ORDER_REFERENCE_FIELD).send_keys(
        root.find("order_referance").text)
    main_page.wait_element(*MainPageLocators.CONTACT_US_MESSAGE_FIELD).send_keys(
        root.find('text').text)
    main_page.clicker(*MainPageLocators.SEND_BUTTON)
    confirm = main_page.wait_element(*MainPageLocators.CONFIRM_SUCCESS_MESSAGE_CONTACT_US)
    assert "Your message has been successfully sent to our team." in confirm.text


# поисковик
def test_searcher(open_browser):
    main_page = MainPage(open_browser, root.find("main_url").text)
    main_page.open()
    name_item = main_page.wait_element(*MainPageLocators.ITEM_TEXT).text
    main_page.wait_element(*MainPageLocators.SEARCH_QUERY).send_keys(name_item)
    main_page.clicker(*MainPageLocators.BUTTON_SEARCH)
    result = main_page.wait_element(*MainPageLocators.SEARCH_RESULT).text
    assert name_item == result
