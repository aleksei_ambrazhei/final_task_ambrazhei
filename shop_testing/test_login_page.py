from pages.main_page import MainPage, MainPageLocators
from pages.login_page import LoginPage, LoginPageLocators, LoginPageRegistration
from pages.base_page import root
from selenium.webdriver.support.ui import Select
import string
import random


def test_login_page(open_browser):
    login_page = MainPage(open_browser, root.find("main_url").text)
    login_page.open()
    login_page.clicker(*MainPageLocators.LOGIN)
    login_page = LoginPage(open_browser, open_browser.current_url)
    login_page.should_be_login_url()
    login_page.should_be_authentication(*LoginPageLocators.AUTHENTICATION)
    login_page.should_be_create_button(*LoginPageLocators.CREATE_AN_ACCOUNT_BUTTON)
    login_page.should_be_sign_in_button(*LoginPageLocators.SIGN_IN_BUTTON)


def test_alert_message_empty_email(open_browser):
    login_page = MainPage(open_browser, root.find("main_url").text)
    login_page.open()
    login_page.clicker(*MainPageLocators.LOGIN)
    login_page.clicker(*LoginPageLocators.CREATE_AN_ACCOUNT_BUTTON)
    confirm = login_page.wait_element(*LoginPageLocators.
                                      EMPTY_OR_INCORRECT_EMAIL_IN_CREATE_AN_ACCOUNT)
    assert "Invalid email address." in confirm.text


def test_alert_message_correct_email(open_browser):
    login_page = MainPage(open_browser, root.find("main_url").text)
    login_page.open()
    login_page.clicker(*MainPageLocators.LOGIN)
    email = login_page.wait_element(*LoginPageLocators.EMAIL_FIELD)
    email.send_keys('aambrazhei@gmail.com')
    login_page.clicker(*LoginPageLocators.CREATE_AN_ACCOUNT_BUTTON)
    button = login_page.wait_element(*LoginPageLocators.SECOND_REGISTRATION_PAGE)
    assert "Register" in button.text


def test_alert_incorrect_email(open_browser):
    login_page = MainPage(open_browser, root.find("main_url").text)
    login_page.open()
    login_page.clicker(*MainPageLocators.LOGIN)
    email = login_page.wait_element(*LoginPageLocators.EMAIL_FIELD)
    incorrect_email = ''
    x = 0
    general_list = string.ascii_letters + string.punctuation
    while x < 255:
        for i in random.choice(general_list):
            incorrect_email += i
            x += 1
    email.send_keys(incorrect_email)
    login_page.clicker(*LoginPageLocators.CREATE_AN_ACCOUNT_BUTTON)
    confirm = login_page.wait_element(
        *LoginPageLocators.EMPTY_OR_INCORRECT_EMAIL_IN_CREATE_AN_ACCOUNT)
    assert "Invalid email address." in confirm.text


# проверка сообщения об ощибке, валидные данные, незарегистрированный юзер
def test_registration_form_with_nonregistered_user(open_browser):
    login_page = MainPage(open_browser, root.find('main_url').text)
    login_page.open()
    login_page.clicker(*MainPageLocators.LOGIN)
    email = login_page.wait_element(*LoginPageLocators.REGISTERED_EMAIL)
    email.send_keys(login_page.generate_email())
    passwd = login_page.wait_element(*LoginPageLocators.REGISTERED_PASSWD)
    passwd.send_keys(root.find("password").text)
    login_page.clicker(*LoginPageLocators.BUTTON_SUBMIT_LOGIN)
    confirm = login_page.wait_element(*LoginPageLocators.
                                      EMPTY_OR_INCORRECT_EMAIL_IN_CREATE_AN_ACCOUNT)
    assert "Authentication failed." in confirm.text


# регистрация нового юзера
def test_registration_new_user(open_browser):
    login_page = MainPage(open_browser, root.find('main_url').text)
    login_page.open()
    login_page.clicker(*MainPageLocators.LOGIN)
    register = login_page.wait_element(*LoginPageLocators.EMAIL_FIELD)
    register.send_keys(login_page.generate_email())
    login_page.clicker(*LoginPageLocators.CREATE_AN_ACCOUNT_BUTTON)
    login_page.wait_element(*LoginPageRegistration.GENDER).click()
    login_page.wait_element(*LoginPageRegistration.FIRST_NAME).send_keys(
        f"{root.find('first_name').text}")
    login_page.wait_element(*LoginPageRegistration.LAST_NAME).send_keys(
        f"{root.find('last_name').text}")
    login_page.wait_element(*LoginPageRegistration.PASSWORD_FIELD).send_keys(
        f"{root.find('password').text}")
    login_page.wait_element(*LoginPageRegistration.ADDRESS).send_keys(
        f"{root.find('address').text}")
    login_page.wait_element(*LoginPageRegistration.CITY).send_keys(f"{root.find('city').text}")
    state = Select(open_browser.find_element_by_xpath("//select[@name='id_state']"))
    state.select_by_value(f"{root.find('id_state').text}")
    login_page.wait_element(*LoginPageRegistration.ZIP_CODE).send_keys(
        f"{root.find('zip_code').text}")
    login_page.wait_element(*LoginPageRegistration.PHONE).send_keys(f"{root.find('phone').text}")
    login_page.wait_element(*LoginPageRegistration.ASSIGN_FUTURE_ALIAS).send_keys(
        f"{root.find('second_adress').text}")
    login_page.clicker(*LoginPageRegistration.REGISTER_BUTTON)
    confirm = login_page.wait_element(*LoginPageRegistration.SUCCESS_REGISTRATION)
    assert "Sign out" in confirm.text


# проверка что мы можем зайти под зарегистрированным юзером
def test_registered_user(open_browser):
    login_page = MainPage(open_browser, root.find('main_url').text)
    login_page.open()
    login_page.clicker(*MainPageLocators.LOGIN)
    login_page.wait_element(*LoginPageLocators.REGISTERED_EMAIL).send_keys(
        f'{root.find("email_for_registration").text}')
    login_page.wait_element(*LoginPageLocators.REGISTERED_PASSWD).send_keys(
        f'{root.find("password").text}')
    login_page.clicker(*LoginPageLocators.BUTTON_SUBMIT_LOGIN)
    confirm = login_page.wait_element(*LoginPageRegistration.SUCCESS_REGISTRATION)
    assert "Sign out" in confirm.text


# проверка входа и выхода из аккаунта
def test_in_out(open_browser):
    authorization = MainPage(open_browser, root.find("main_url").text)
    authorization.open()
    authorization.log_in()
    authorization.wait_element(*LoginPageRegistration.SUCCESS_REGISTRATION).click()
    confirm = authorization.wait_element(*MainPageLocators.LOGIN)
    assert "Sign in" in confirm.text
