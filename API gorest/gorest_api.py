import requests
import json


def send_request(method, url, headers=None, params=None, data=None, expected_status=200) -> object:
    response = requests.request(method=method, url=url, headers=headers, params=params, data=data)
    assert response.status_code == expected_status, 'something wrong, check status_code! ' \
                                                    f'{response.status_code}'
    return response


with open('data.json', 'r') as file:
    data = json.load(file)


# cписок юзеров
def users_list():
    response = send_request("get", f'{data["base_url"]}users?page=22')
    print(json.dumps(response.json(), indent=4))


# список постов
def posts_list():
    response = send_request("get", f'{data["base_url"]}posts')
    print(json.dumps(response.json(), indent=4))


# список категорий
def categories_list():
    response = send_request("get", f'{data["base_url"]}categories')
    print(json.dumps(response.json(), indent=4))


with open('user_data.json', 'r') as file:
    body = json.load(file)


# создаем юзера с валидными данными
def create_user(body):
    response = send_request("POST", f'{data["base_url"]}users',

                            headers={"Host": "gorest.co.in",
                                     "Accept": "application/json",
                                     "Content-Type": "application/json",
                                     "Authorization": f"Bearer {data['access_token']}"},
                            data=json.dumps(body))
    print(json.dumps(response.json(), indent=4))
    assert response.json()['code'] == 201, f'201 user has been created, current code ' \
                                           f'is {response.json()["code"]}' \
                                           f' if 422 email has already been taken'


# доступ по неправильному токену
def incorrect_token():
    response = send_request("POST", f'{data["base_url"]}users',

                            headers={"Host": "gorest.co.in",
                                     "Accept": "application/json",
                                     "Content-Type": "application/json",
                                     "Authorization": f"Bearer {data['incorrect_access_token']}"})
    print(response.json())
    assert response.json()["code"] == 401, f'401 for Authentication failed, ' \
                                           f'current code is {response.json()["code"]}'


# удаление юзера
def delete_user():
    response = send_request("DELETE", f'{data["base_url"]}users/192',

                            headers={"Host": "gorest.co.in",
                                     "Accept": "application/json",
                                     "Content-Type": "application/json",
                                     "Authorization": f"Bearer {data['access_token']}"})

    print(json.dumps(response.json(), indent=4))
    assert response.json()["code"] == 204, "204: The request was handled successfully " \
                                           "and the response contains no body content " \
                                           f"(like a DELETE request), " \
                                           f"current code is {response.json()['code']}"


# Update user details
def update_user(body):
    response = send_request("PUT", f'{data["base_url"]}users/123',

                            headers={"Host": "gorest.co.in",
                                     "Accept": "application/json",
                                     "Content-Type": "application/json",
                                     "Authorization": f"Bearer {data['access_token']}"},
                            data=json.dumps(body))
    print(json.dumps(response.json(), indent=4))
    assert response.json()['code'] == 200, f"current code is {response.json()['code']}"


users_list()
posts_list()
categories_list()
create_user(body)
incorrect_token()
delete_user()
update_user(body)
